<?php
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// T
	'theme_bsblog_description' => 'Blog simple, 2 colonnes, responsive, aux couleurs douces.',
	'theme_bsblog_slogan' => 'Blog simple, 2 colonnes, responsive, aux couleurs douces',
);
